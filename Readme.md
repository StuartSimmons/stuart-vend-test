# Architecture
Simple MVVM + Repository for db interaction
Hilt for DI
Compose for UI

# The good
Animations 
Dark mode based off system preference

# The bad
Material3 is pretty immature, some components need to be styled manually
I didn't get to implementing error states

# Notes
I went with sort ordering for the "reordering" requirement. Jetpack compose lists require manual manipulation of drag gestures which is doable but would take some time.
I didn't test the viewModel as there currently isn't any logic there, its all handled in the repo
I only did a simple compose test on the dropdown, coverage of the empty state logic on the main screen would have been nice