package nz.stu.vendtest.feature.imagelist

import app.cash.turbine.test
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import nz.stu.vendtest.data.local.ImagesDao
import nz.stu.vendtest.data.local.SortOrder
import nz.stu.vendtest.data.remote.ImageService
import nz.stu.vendtest.data.remote.model.ImageResponse
import nz.stu.vendtest.data.remote.model.toCachedImage
import org.junit.Assert.assertEquals
import org.junit.Test

@ExperimentalCoroutinesApi
class ImagesRepositoryKtTest {

    private val mockImageService: ImageService = mockk()
    private val mockImagesDao: ImagesDao = mockk()
    private val repo = ImagesRepository(
        imageService = mockImageService,
        imagesDao = mockImagesDao
    )

    private val mockImages = listOf(
        ImageResponse(
            id = "1",
            url = "https://test.test",
            author = "Legend",
            height = 100,
            width = 100,
            downloadUrl = ""
        ),
        ImageResponse(
            id = "2",
            url = "https://test.test",
            author = "Legend",
            height = 100,
            width = 100,
            downloadUrl = ""
        )
    )

    @Test
    fun `repository emits items`() = runBlocking {
        val cachedImages = mockImages.map { it.toCachedImage() }.toList()

        val mockCachedImageFlow = flowOf(cachedImages)

        coEvery { mockImageService.getRandomImages() } returns mockImages
        coEvery { mockImagesDao.getImagesSortById() } returns mockCachedImageFlow

        repo.imageListFlow.test {
            assertEquals(awaitItem(), cachedImages)
            cancelAndConsumeRemainingEvents()
        }
    }

    @Test
    fun `sort order change returns correct flow`() = runBlocking {
        val cachedImages = mockImages.map { it.toCachedImage() }.toList()

        val mockCachedImageFlow = flowOf(cachedImages)
        val mockCachedImageAlphabeticalFlow = flowOf(cachedImages.sortedBy { it.author })

        coEvery { mockImageService.getRandomImages() } returns mockImages
        coEvery { mockImagesDao.getImagesSortById() } returns mockCachedImageFlow
        coEvery { mockImagesDao.getImagesSortAlphabetically() } returns mockCachedImageAlphabeticalFlow

        repo.updateSortOrder(SortOrder.ALPHABETICAL)

        repo.imageListFlow.test {
            val response = awaitItem()
            assertEquals(response[0].author, "Absolute Legend")
            assertEquals(response[1].author, "Legend")
            cancelAndConsumeRemainingEvents()
        }
    }

}