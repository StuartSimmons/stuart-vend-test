package nz.stu.vendtest.feature.imagelist

import org.junit.Assert
import org.junit.Test

class RandomNumberTest {
    @Test
    fun `random number generator with list returns int`() {
        val items = listOf(1,2,3)
        val randomItem = items.takeRandom()
        Assert.assertTrue(items.contains(randomItem))
    }

    @Test
    fun `random number generator empty list throws error`() {
        val items = emptyList<Int>()
        Assert.assertThrows(IllegalArgumentException::class.java) { items.takeRandom() }
    }

}