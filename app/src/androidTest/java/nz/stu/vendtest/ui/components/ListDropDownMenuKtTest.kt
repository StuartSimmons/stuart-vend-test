package nz.stu.vendtest.ui.components

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertValueEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import nz.stu.vendtest.data.local.SortOrder
import nz.stu.vendtest.ui.theme.AppTheme
import org.junit.Rule
import org.junit.Test

class ListDropDownMenuKtTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun dropDownMenuShowsItems() {
        composeTestRule.setContent {
            AppTheme {
                SortOrderDropDownMenu(
                    selectedIndex = 0,
                    selectionItems = listOf(SortOrder.ALPHABETICAL, SortOrder.ID),
                    onSortOrderChanged = {},
                    onShowChanged = {},
                    showMenu = true
                )
            }
        }

        composeTestRule.onNodeWithText(SortOrder.ALPHABETICAL.displayName).assertIsDisplayed()
        composeTestRule.onNodeWithText(SortOrder.ID.displayName).assertIsDisplayed()
    }
}