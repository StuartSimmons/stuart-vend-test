package nz.stu.vendtest.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import nz.stu.vendtest.data.local.model.CachedImage

@Database(entities = [CachedImage::class], version = 1)
abstract class ImageDatabase : RoomDatabase() {
    abstract fun imagesDao(): ImagesDao
}