package nz.stu.vendtest.data.local.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CachedImage(
    @PrimaryKey val id: String,
    @NonNull @ColumnInfo(name = "position") val position: Int,
    @NonNull @ColumnInfo(name = "author") val author: String,
    @NonNull @ColumnInfo(name = "url") val url: String
)