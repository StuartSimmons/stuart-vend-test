package nz.stu.vendtest.data.remote

import nz.stu.vendtest.data.remote.model.ImageResponse
import retrofit2.http.GET

interface ImageService {
    @GET("v2/list")
    suspend fun getRandomImages(): List<ImageResponse>
}