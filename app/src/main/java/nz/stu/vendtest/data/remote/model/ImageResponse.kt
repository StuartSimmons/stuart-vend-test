package nz.stu.vendtest.data.remote.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import nz.stu.vendtest.data.local.model.CachedImage

@Serializable
data class ImageResponse(
    val id: String,
    val author: String,
    val width: Long,
    val height: Long,
    val url: String,

    @SerialName("download_url")
    val downloadUrl: String
)

fun ImageResponse.toCachedImage(): CachedImage {
    return CachedImage(
        id = this.id,
        author = this.author,
        url = this.downloadUrl,
        position = 0
    )
}