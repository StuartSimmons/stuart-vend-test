package nz.stu.vendtest.data.local

enum class SortOrder(val displayName: String) {
    ALPHABETICAL("Alphabetical"),
    ID("Id")
}