package nz.stu.vendtest.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import nz.stu.vendtest.data.local.model.CachedImage
import androidx.room.OnConflictStrategy

@Dao
interface ImagesDao {
    @Query("SELECT * FROM CachedImage ORDER BY id ASC")
    fun getImagesSortById(): Flow<List<CachedImage>>

    @Query("SELECT * FROM CachedImage ORDER BY author ASC")
    fun getImagesSortAlphabetically(): Flow<List<CachedImage>>

    @Query("DELETE FROM CachedImage WHERE id = :id")
    fun delete(id: String)

    @Query("DELETE FROM CachedImage")
    fun clear()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertImage(image: CachedImage)
}