package nz.stu.vendtest.di

import android.content.Context
import androidx.room.Room
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import nz.stu.vendtest.data.local.ImageDatabase
import nz.stu.vendtest.data.local.ImagesDao
import nz.stu.vendtest.data.remote.ImageService
import nz.stu.vendtest.feature.imagelist.ImagesRepository
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@ExperimentalSerializationApi
@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun providesOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        return builder.build()
    }

    @Provides
    @Singleton
    fun providesRetrofit(okHttpClient: OkHttpClient): Retrofit {
        val contentType = "application/json".toMediaType()

        return Retrofit.Builder().baseUrl("https://picsum.photos/")
            .client(okHttpClient)
            .addConverterFactory(Json.asConverterFactory(contentType))
            .build()
    }

    @Provides
    @Singleton
    fun providesImageRepository(
        imageService: ImageService,
        imagesDao: ImagesDao
    ): ImagesRepository {
        return ImagesRepository(imageService, imagesDao)
    }

    @Provides
    @Singleton
    fun providesImageService(
        retrofit: Retrofit,
    ): ImageService {
        return retrofit.create(ImageService::class.java)
    }

    @Provides
    fun provideImagesDao(appDatabase: ImageDatabase): ImagesDao {
        return appDatabase.imagesDao()
    }

    @Provides
    @Singleton
    fun provideImageDatabase(@ApplicationContext appContext: Context): ImageDatabase {
        return Room.databaseBuilder(
            appContext,
            ImageDatabase::class.java,
            "ImageDatabase"
        ).build()
    }
}