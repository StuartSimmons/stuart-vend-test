package nz.stu.vendtest.feature.imagelist

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.MutableTransitionState
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import kotlinx.coroutines.ExperimentalCoroutinesApi
import nz.stu.vendtest.R
import nz.stu.vendtest.data.local.SortOrder
import nz.stu.vendtest.ui.components.AppBar
import nz.stu.vendtest.ui.components.ImageCard
import nz.stu.vendtest.ui.components.NoImagesState

@ExperimentalAnimationApi
@ExperimentalCoroutinesApi
@ExperimentalMaterial3Api
@Composable
fun ImageListScreen(
    viewModel: ImageViewModel = viewModel()
) {
    val systemUiController = rememberSystemUiController()
    val useDarkIcons = !isSystemInDarkTheme()
    val surfaceColour = MaterialTheme.colorScheme.surface

    SideEffect {
        systemUiController.setStatusBarColor(
            color = surfaceColour,
            darkIcons = useDarkIcons
        )
    }

    val images = viewModel.imageList.observeAsState()
    val sortBy = viewModel.currentSortOrder.observeAsState()
    val showEmptyState: Boolean = images.value?.isEmpty() ?: true

    Scaffold(
        topBar = {
            AppBar(
                onClear = {
                    viewModel.clearAll()
                },
                onSortOrderChanged = {
                    viewModel.onSortOrderChanged(it)
                },
                currentSortOrder = sortBy.value ?: SortOrder.ID
            )
        },
        floatingActionButton = {
            AnimatedVisibility(
                visible = !showEmptyState,
                enter = slideInVertically(initialOffsetY = { 300 }),
                exit = slideOutVertically(targetOffsetY = { 300 })
            ) {
                ExtendedFloatingActionButton(
                    text = { Text(stringResource(id = R.string.add_image_button_label)) },
                    icon = { Icon(Icons.Filled.Add, "add") },
                    onClick = {
                        viewModel.loadRandomImage()
                    }
                )
            }
        }
    ) {
        Column(
            Modifier.verticalScroll(rememberScrollState())
        ) {
            images.value?.let {
                if (showEmptyState) {
                    NoImagesState {
                        viewModel.loadRandomImage()
                    }
                } else {
                    images.value?.forEach { image ->
                        ImageCard(
                            url = image.url,
                            author = image.author,
                            onDelete = {
                                viewModel.deleteImage(image)
                            }
                        )
                    }
                }
            }
        }
    }
}
