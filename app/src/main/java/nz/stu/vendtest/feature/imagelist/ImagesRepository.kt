package nz.stu.vendtest.feature.imagelist

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.withContext
import nz.stu.vendtest.data.local.ImagesDao
import nz.stu.vendtest.data.local.SortOrder
import nz.stu.vendtest.data.local.model.CachedImage
import nz.stu.vendtest.data.remote.ImageService
import nz.stu.vendtest.data.remote.model.ImageResponse
import nz.stu.vendtest.data.remote.model.toCachedImage
import javax.inject.Inject
import kotlin.random.Random

@ExperimentalCoroutinesApi
class ImagesRepository @Inject constructor(
    private val imageService: ImageService,
    private val imagesDao: ImagesDao
) {
    val sortFlow = MutableStateFlow(SortOrder.ID)

    val imageListFlow = sortFlow
        .flatMapLatest { sortOrder ->
            when (sortOrder) {
                SortOrder.ALPHABETICAL -> imagesDao.getImagesSortAlphabetically()
                SortOrder.ID -> imagesDao.getImagesSortById()
            }
        }

    suspend fun updateSortOrder(sortBy: SortOrder) {
        sortFlow.emit(sortBy)
    }

    suspend fun getRandomImage() {
        try {
            val image = imageService.getRandomImages().takeRandom()
            saveImage(image)
        } catch (e: IllegalArgumentException) {
            // todo error state
        }
    }

    private suspend fun saveImage(image: ImageResponse) = withContext(Dispatchers.IO) {
        imagesDao.insertImage(image.toCachedImage())
    }

    suspend fun clearAll() = withContext(Dispatchers.IO) {
        imagesDao.clear()
    }

    suspend fun deleteImage(image: CachedImage) = withContext(Dispatchers.IO) {
        imagesDao.delete(image.id)
    }
}

fun <E> List<E>.takeRandom(): E {
    if (this.isNullOrEmpty()) {
        throw IllegalArgumentException()
    }
    val randomIndex = Random.nextInt(0, this.size - 1)
    return this[randomIndex]
}
