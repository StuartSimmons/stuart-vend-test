package nz.stu.vendtest.feature.imagelist

import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import nz.stu.vendtest.data.local.SortOrder
import nz.stu.vendtest.data.local.model.CachedImage
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class ImageViewModel @Inject constructor(
    private val imagesRepository: ImagesRepository
) : ViewModel() {

    val imageList: LiveData<List<CachedImage>> = imagesRepository.imageListFlow.asLiveData()

    val currentSortOrder: LiveData<SortOrder> = imagesRepository.sortFlow.asLiveData()

    fun loadRandomImage() {
        viewModelScope.launch {
            imagesRepository.getRandomImage()
        }
    }

    fun clearAll() {
        viewModelScope.launch {
            imagesRepository.clearAll()
        }
    }

    fun onSortOrderChanged(sortBy: SortOrder) {
        viewModelScope.launch {
            imagesRepository.updateSortOrder(sortBy)
        }
    }

    fun deleteImage(image: CachedImage) {
        viewModelScope.launch {
            imagesRepository.deleteImage(image)
        }
    }
}
