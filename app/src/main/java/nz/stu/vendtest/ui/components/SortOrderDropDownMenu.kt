package nz.stu.vendtest.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.selection.selectable
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import nz.stu.vendtest.data.local.SortOrder

@Composable
fun SortOrderDropDownMenu(
    showMenu: Boolean,
    onShowChanged: (Boolean) -> Unit,
    onSortOrderChanged: (SortOrder) -> Unit,
    selectionItems: List<SortOrder>,
    selectedIndex: Int
) {
    DropdownMenu(
        expanded = showMenu,
        onDismissRequest = { onShowChanged(false) },
        modifier = Modifier.background(color = MaterialTheme.colorScheme.surface)
    ) {
        selectionItems.forEachIndexed { index, item ->
            DropdownMenuItem(
                onClick = {
                    onSortOrderChanged(item)
                    onShowChanged(false)
                },
                Modifier
                    .fillMaxWidth()
                    .background(
                        color = if (index == selectedIndex) {
                            MaterialTheme.colorScheme.surfaceVariant
                        } else {
                            MaterialTheme.colorScheme.surface
                        }
                    )
            ) {
                Text(
                    text = item.displayName
                )
            }
        }
    }
}