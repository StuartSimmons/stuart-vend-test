package nz.stu.vendtest.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Sort
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import nz.stu.vendtest.data.local.SortOrder

@Composable
fun AppBar(
    onClear: () -> Unit,
    onSortOrderChanged: (SortOrder) -> Unit,
    currentSortOrder: SortOrder
) {
    var showMenu by remember { mutableStateOf(false) }
    val menuItems = listOf(SortOrder.ALPHABETICAL, SortOrder.ID)

    SmallTopAppBar(
        title = {
            Row(
                horizontalArrangement = Arrangement.End,
                modifier = Modifier.fillMaxWidth()
            ) {
                IconButton(
                    onClick = {
                        onClear()
                    }
                ) {
                    Icon(
                        Icons.Filled.Delete,
                        contentDescription = "Delete"
                    )
                }
                IconButton(
                    onClick = {
                        showMenu = true
                    }
                ) {
                    Icon(
                        Icons.Filled.Sort,
                        contentDescription = "Sort"
                    )

                    SortOrderDropDownMenu(
                        showMenu = showMenu, onShowChanged = {
                            showMenu = it
                        },
                        onSortOrderChanged = {
                            onSortOrderChanged(it)
                        },
                        selectionItems = menuItems,
                        selectedIndex = menuItems.indexOf(currentSortOrder)
                    )
                }
            }
        })
}
