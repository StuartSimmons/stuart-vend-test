package nz.stu.vendtest.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material3.FilledTonalButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.airbnb.lottie.compose.*
import nz.stu.vendtest.R

@Composable
fun NoImagesState(onClickLoad: () -> Unit) {
    val composition by rememberLottieComposition(
        LottieCompositionSpec.RawRes(
            R.raw.photographer
        )
    )
    val progress by animateLottieCompositionAsState(
        composition,
        iterations = LottieConstants.IterateForever,
    )

    Column(
        modifier = Modifier
            .padding(16.dp)
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        LottieAnimation(
            composition = composition,
            modifier = Modifier.height(350.dp),
            progress = progress
        )
        Text(
            text = stringResource(id = R.string.no_images),
            fontSize = 16.sp
        )
        FilledTonalButton(
            onClick = { onClickLoad() },
            modifier = Modifier.padding(16.dp)
        ) {
            Text(
                text = stringResource(id = R.string.add_first_image_label)
            )
        }
    }
}
